import RouterInfo from '../viewmodel/RouterInfo'
export const  routers:RouterInfo[]=[
  new RouterInfo("任务进度页面",'pages/JobPage'),
  new RouterInfo("任务列表的持久化操作",'pages/TaskManagePage'),
  new RouterInfo("通知","pages/NotificationPage"),
  new RouterInfo("应用状态","pages/CinemaPage"),
]
