import relationalStore from '@ohos.data.relationalStore';
class EmployeeModel{
  store:relationalStore.RdbStore
  //使用关系型数据库实现数据持久化，需要获取一个RdbStore
  async loadRdbStore(context){
    const STORE_CONFIG = {
      name: 'RdbTest.db', // 数据库文件名
      securityLevel: relationalStore.SecurityLevel.S1 // 数据库安全级别
    };

    const SQL_CREATE_TABLE = 'CREATE TABLE IF NOT EXISTS EMPLOYEE (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL, AGE INTEGER, SALARY REAL, CODES BLOB)'; // 建表Sql语句

   await relationalStore.getRdbStore(context, STORE_CONFIG, (err, store) => {
      if (err) {
        console.error('testTag',`Failed to get RdbStore. Code:${err.code}, message:${err.message}`);
        return;
      }
      store.executeSql(SQL_CREATE_TABLE)
        .then(()=>{
          this.store=store
          console.info('testTag',`Succeeded in getting RdbStore.`);
        })
        .catch(err=>{
          console.info('testTag',`error in getting RdbStore.`);
        })


    })
  }

  //添加
 addEmployee(){
    //ID, NAME , AGE,SALARY, CODES)
    const valueBucket = {
      'NAME': 'Lisa',
      'AGE': 18,
      'SALARY': 100.5,
      'CODES': new Uint8Array([1, 2, 3, 4, 5])
    };
    this.store.insert('EMPLOYEE', valueBucket, (err, rowId) => {
      if (err) {
        console.error('testTag',`Failed to insert data. Code:${err.code}, message:${err.message}`);
      }
      console.info('testTag',`Succeeded in inserting data. rowId:${rowId}`);
      return
    })

  }
 //修改
  updateEmployee(){
    const valueBucket = {
      'NAME': 'Rose',
      'AGE': 22,
      'SALARY': 200.5,
      'CODES': new Uint8Array([1, 2, 3, 4, 5])
    };
    //PreparedStatement
    let predicates = new relationalStore.RdbPredicates('EMPLOYEE'); // 创建表'EMPLOYEE'的predicates
    predicates.equalTo('NAME', 'Lisa'); // 匹配表'EMPLOYEE'中'NAME'为'Lisa'的字段
    this.store.update(valueBucket, predicates, (err, rows) => {
      if (err) {
        console.error('testTag',`Failed to update data. Code:${err.code}, message:${err.message}`);
        return;
      }
      console.info('testTag',`Succeeded in updating data. row count: ${rows}`);
    })
  }


//删除
  deleteEmployee(){
    let predicates = new relationalStore.RdbPredicates('EMPLOYEE');
    predicates.equalTo('NAME', 'Rose');
    this.store.delete(predicates, (err, rows) => {
      if (err) {
        console.error('testTag',`Failed to delete data. Code:${err.code}, message:${err.message}`);
        return;
      }
      console.info('testTag',`Delete rows: ${rows}`);
    })
  }
  //查询
  queryEmployee(){
    let predicates = new relationalStore.RdbPredicates('EMPLOYEE');
    predicates.equalTo('NAME', 'Rose');
   this.store.query(predicates, ['ID', 'NAME', 'AGE', 'SALARY', 'CODES'], (err, resultSet) => {
      if (err) {
        console.error('testTag',`Failed to query data. Code:${err.code}, message:${err.message}`);
        return;
      }
      console.info('testTag',`ResultSet column names: ${resultSet.columnNames}`);
      console.info('testTag',`ResultSet column count: ${resultSet.columnCount}`);

      while(!resultSet.isAtLastRow){
        resultSet.goToNextRow()
        let id=resultSet.getLong(resultSet.getColumnIndex("ID"))
        let name=resultSet.getString(resultSet.getColumnIndex("NAME"))
        let age=resultSet.getLong(resultSet.getColumnIndex("AGE"))
        let salary=resultSet.getDouble(resultSet.getColumnIndex("SALARY"))
        let codes=resultSet.getBlob(resultSet.getColumnIndex("CODES"))
        console.log('testTag',`id:${id},name：${name},age:${age},salary:${salary},codes:${codes}`)
      }
    })
  }






}
const employeeModel=new EmployeeModel()
export default employeeModel as EmployeeModel;